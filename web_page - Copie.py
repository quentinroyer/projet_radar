import cherrypy
from pymongo import Connection
class mongocherry(object):
#https://paulcrickard.wordpress.com/2012/11/28/d3-js-and-mongodb/
#mongoexport --host localhost --db dbname --collection name --type=csv --out text.csv --fields annee,Radar fixe, Radar Feu Rouge, Radar discriminant, Radar passage à niveau, Radar Vitesse Moyenne, Itinéraire
def index(self):
db=Connection().geo
output =[]
output.append(‘<html>
<head>
  <meta charset="utf-8">
  <title>Evolution du nombre de radar</title>
  <style>
    body {
      font-family: 'Open Sans', sans-serif;
    }
    #main {
      width: 960px;
    }
    .axis .domain {
      display: none;
    }
    </style>
  <script src="https://d3js.org/d3.v4.min.js"></script>
  <link rel="stylesheet" href="./bootstrap-4.1.3-dist/css/bootstrap.min.css">
  <script src="./bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
        <a class="navbar-brand" href="#">Projet de visualisation</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
            <a class="nav-link" href="./index.html">Home</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="./stacked.html">Stacked bar chart</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="./barchart.html">Bar chart</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="./map.html">Map</a>
            </li>
        </ul>
        </div>
</nav>
  <div id="main">
    <svg width="960" height="500"></svg>
  </div>’)
output.append(‘<script type=”text/javascript”>var dataset=[‘)
for x in db.places.find():
output.append(str(x[“loc”][0])+’,’)
output.append(‘0];’+”\n”+’d3.select(“body”).selectAll(“div”).data(dataset).enter().append(“div”).attr(“class”, “bar”).style(“height”, function(d) {var barHeight = d * 5;return barHeight + “px”;});</script></body></html>’)
i=0
html=””
while i<len(output):
html+=str(output[i])
i+=1
return html
index.exposed = True
cherrypy.config.update({‘server.socket_host’: ‘127.0.0.1’,
‘server.socket_port’: 8000,
})
cherrypy.quickstart(mongocherry())