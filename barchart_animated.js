var w = 300, h = 300;
var margin = 200; var width = 900;
var height = 500;
var donne = {};
var csv2 = {};
var popula = {};
var new_array = {};
var test = {};
var nbelement = 0;
var newt = {} ;
var svg = d3.select("#barchart").append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("class", "YlOrRd"); //Changer la class de la categorie de couleurs

svg = d3.select("svg"),
    margin = 200,
    width = svg.attr("width") - margin,
    height = svg.attr("height") - margin;

svg.append("text")
    .attr("transform", "translate(100,0)")
    .attr("x", 50)
    .attr("y", 50)
    .attr("font-size", "24px")
    .text("Evolution du nombre de radars par an")

var x = d3.scaleBand().range([0, width]).padding(0.4),
    y = d3.scaleLinear().range([height, 0]);

var g = svg.append("g")
    .attr("transform", "translate(" + 100 + "," + 100 + ")");

    var g2 ;



d3.csv("https://raw.githubusercontent.com/quentinroyer8/radar/master/nbrederadarannee.csv", function (error, data) {
    if (error) {
        throw error;
    }

    //console.log(data[0].annee_installation);
    // data.forEach(function(e, i)   {  (    console.log(e.annee_installation)) })  ;
    x.domain(data.map(function (d) { return d.annee_installation; }));
    y.domain([0, 500]);

    g.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x))
        .append("text")
        .attr("y", height - 250)
        .attr("x", width - 100)
        .attr("text-anchor", "end")
        .attr("stroke", "black")
        .text("Annee d installation");

    g.append("g")
        .call(d3.axisLeft(y).tickFormat(function (d) {
            return d;
        }).ticks(10))
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-5.1em")
        .attr("text-anchor", "end")
        .attr("stroke", "black")
        .text("Nombre de radar");

    g.selectAll(".bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .on("mouseover", onMouseOver) //Add listener for the mouseover event
        .on("mouseout", onMouseOut)   //Add listener for the mouseout event
        .on("click", onclick)
        // .on("contextmenu",d3.select("#chart").remove())

        .attr("x", function (d) { return x(d.annee_installation); })
        .attr("y", function (d) { return y(d.count); })
        .attr("width", x.bandwidth())
        .transition()
        .ease(d3.easeLinear)
        .duration(400)
        .delay(function (d, i) {
            return i * 50;
        })


        .attr("height", function (d) { return height - y(d.count); })

});

//mouseover event handler function
function onMouseOver(d, i) {
    d3.select(this).attr('class', 'highlight');
    d3.select(this)
        .transition()     // adds animation
        .duration(400)
        .attr('width', x.bandwidth() + 5)
        .attr("y", function (d) { return y(d.count) - 10; })
        .attr("height", function (d) { return height - y(d.count) + 10; });

    g.append("text")
        .attr('class', 'val')
        .attr('x', function () {
            return x(d.annee_installation);
        })
        .attr('y', function () {
            return y(d.count) - 15;
        })
        .text(function () {
            return [d.count];  // count of the text
        });
    console.log(i)
}

//mouseout event handler function
function onMouseOut(d, i) {
    // use the text label class to remove label on mouseout
    d3.select(this).attr('class', 'bar');
    d3.select(this)
        .transition()     // adds animation
        .duration(400)
        .attr('width', x.bandwidth())
        .attr("y", function (d) { return y(d.count); })
        .attr("height", function (d) { return height - y(d.count); });

    d3.selectAll('.val')
        .remove()

}

var pien = function (data2) {
    var width = 960,
        height = 500,
        radius = Math.min(width, height) / 2;

    var color = d3.scaleOrdinal()
        .range(["#98abc5", "#8a89a6", "#7b6888"]);

    var arc = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var labelArc = d3.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 40);

    var pie = d3.pie()
        .sort(null)
        .value(function (d) { return d.radars; });

    var svg2 = d3.select("#chart").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var g2 = svg2.selectAll(".arc")
        .data(pie(data2))
        .enter().append("g")
        .attr("class", "arc")
         ;
    g2.append("path")
        .attr("d", arc)
        .style("fill", function (d, i) { return color(i); Console.log("hfdhfd") });

        g2.append("text")
        .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
        .attr("dy", "1.2em")
        
        .text(function(d) { return d.data.vitesses; });
}

    function onclick(d, i) {
     // g2.selectAll("#chart").remove();
    // d3.select("svg2").remove();
    
        d3.csv("https://raw.githubusercontent.com/quentinroyer8/radar/master/vitesse.csv", function (data1) {

            data1.forEach(function (e, j) { (popula[j] = e); nbelement = j });

            console.log(nbelement);

            new_array = {}  // reinitialisation du tableau a chaque filtrage 
            for (var i = 0; i < nbelement; i++)  //filtrage de l'année
            {

                test = (popula[i].annee_installation == d.annee_installation); // filtre sur l'année cliqué 
                console.log("Le test est ", test);
                if (test == true) {
                    console.log(true)
                    new_array[i] =  { "radars": popula[i].departement, "vitesses": popula[i].vitesse_vehicules_legers_kmh } ; // on copie dans e nouveau table les valeurs filtrés 
                  
                }
  
            }
            newt = Object.values(new_array)
// var output = Object.entries(new_array).map(([key, value]) => ({key,value}));
  console.log (newt)

            pien(newt) ;



        })
    }