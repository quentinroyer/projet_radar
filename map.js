
//////////
var tooltip = {};
var popula = 0 ;
///The attributes of the bart chart
var svgWidth = 700;
var svgHeight = 550;
const margin = 50;
const inner = 50;
const outer = 120;



const nbData = 97;
const yScale = d3.scaleLinear()

    .range([svgHeight, 0])
    .domain([0, 100]);

var svg2 = d3.select('#map2').append("svg")
    .attr("width", svgWidth)
    .attr("height", svgHeight)
    .attr("class", "donut-chart");


let g = svg2
    .append('g')
    .attr('transform', `translate(${svgWidth / 2}, ${svgHeight / 2})`);

let centre = g.append('circle')
    .attr('cx', 0)
    .attr('cy', 0)
    .attr('r', 5);

let couleurs = [];
let couleurs2 = [];
let delta = 360 / 20;


var tooltip = d3.select('#map2')
    .append('div')
    .attr('class', 'tooltip');


tooltip.append('div')
    .attr('class', 'label');

tooltip.append('div')
    .attr('class', 'count');

tooltip.append('div')
    .attr('class', 'percent');
//function dinut 
let donut = function (data) {
    var tableau = {};
    for (let i = 0; i < 20; i++) {
        couleurs.push(d3.hsl(delta * i, 0.5, 0.6));
        couleurs2.push(d3.hsl(delta * i, 0.9, 0.6));
    }
    var tableau = Object.values(data);



    const sum = d3.sum(tableau);
    const nb = tableau.length;
    let dataAngle = [];
    let angle = 0;
    for (let i = 0; i < nb; i++) {
        dataAngle.push(angle);
        angle += tableau[i] * 360 / sum;
    }
    dataAngle.push(angle);

    let secteurs = g.selectAll('path')
        .data(tableau);

    secteurs.enter()
        .append('path')
        .attr('d', function (d, i) {
            let arc = d3.arc()
                .innerRadius(inner)
                .outerRadius(outer)
                .startAngle(Math.PI * 2 * dataAngle[i] / 360)
                .endAngle(Math.PI * 2 * dataAngle[i + 1] / 360);
            return arc();
        })
        .attr('fill', function (d, i) {
            return couleurs[i];
        })
        .on('mouseover', function (d, i) {
            d3.select(this)
                .attr('fill', couleurs2[i])
                .attr('class', 'd3-tip')


            //console.log("Region" ,  Object.entries(data) [i]); on recupere lindex du tableau dont c'est la tranche
            console.log("Region :", Object.getOwnPropertyNames(data)[i]);
            console.log("Population :", tableau[i]);
            
            tooltip.select('.label').html('<b>Région : </b>' + Object.getOwnPropertyNames(data)[i]);
            tooltip.select('.count').html('<b>Nombre de radars : </b> : ' + tableau[i]);
            tooltip.select('.percent').html('<b>Pourcentage de la population  : </b> : ' +   d3.format(".00%")(tableau[i] / popula));
            tooltip.style('display', 'block');
            tooltip.style('opacity', 2);
            console.log("Le tableau est ", tableau)
            console.log("La somme est ", d3.sum(tableau))
        })
        .on('mousemove', function (d) {
            tooltip.style('top', (d3.event.layerY + 10) + 'px')
                .style('left', (d3.event.layerX - 25) + 'px');
        })
        .on('mouseout', function (d, i) {
            d3.select(this)
                .attr('fill', couleurs[i]);
            tooltip.style('display', 'none');
            tooltip.style('opacity', 0);

        })
        .on('click', function (d, i) {
            tableau.splice(i, 1);
            couleurs.splice(i, 1);
            couleurs2.splice(i, 1);
            donut(tableau);
        });
    /*
    .transition()
    .duration(2000)
    .attr('transform', function(d, i){
        return `rotate(${dataAngle[i]})`
    });*/
    // update
    secteurs.attr('d', function (d, i) {
        let arc = d3.arc()
            .innerRadius(inner)
            .outerRadius(outer)
            .startAngle(Math.PI * 2 * dataAngle[i] / 360)
            .endAngle(Math.PI * 2 * dataAngle[i + 1] / 360);
        return arc();
    })
        .attr('fill', function (d, i) {
            return couleurs[i];
        })

    // Exit
    secteurs.exit().remove();
}


/////////////////donut

var barPadding = 5;


//////////////////////////////////////////// End of prerequisites to the barchart 
const width = 700, height = 550;
var mon_tab = new Array();
const path = d3.geoPath();
var tr = new Array();
var tr2 = new Array();
const projection = d3.geoConicConformal() // Lambert-93
    .center([2.454071, 46.279229]) // Center on France
    .scale(2600)
    .translate([width / 2 - 50, height / 2]);

path.projection(projection);

const svg = d3.select('#map').append("svg")
    .attr("id", "svg")
    .attr("width", width)
    .attr("height", height)
    .attr("class", "YlOrRd"); //Changer la class de la categorie de couleurs

const deps = svg.append("g");

var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);
console.log('les departements')


var promises = [];
promises.push(d3.json("https://raw.githubusercontent.com/quentinroyer8/radar/master/departments.json"));
promises.push(d3.csv("https://raw.githubusercontent.com/quentinroyer8/radar/master/data_carte.csv"));
Promise.all(promises).then(function (values) {
    const geojson = values[0]; // Récupération de la première promesse : le contenu du fichier JSON
    const csv = values[1]; // Récupération de la deuxième promesse : le contenu du fichier csv
    console.log(csv);
    var features = deps
        .selectAll("path")
        .data(geojson.features)
        .enter()
        .append("path")
        .attr('id', function (d) { return "d" + d.properties.CODE_DEPT; })
        .attr("d", path);
    // Ici on insèrera tout le code qui va suivre dans cette partie.
    var populationtotale = csv.map(function (d) { return d.col1 }); // pour determiner le pourcentage au niveau du donut
    console.log(csv)
    
    popula = parseInt(popula)
     csv.forEach(function(e, i)   {  (     popula +=  parseInt(e.POP)  ) ;
    console.log( popula )})   ; // La variable popupla représente le nombre total de radars 

    var quantile = d3.scaleQuantile()
        .domain([0, d3.max(csv, function (e) { return +e.POP; })])
        .range(d3.range(9));
    var legend = svg.append('g')
        .attr('transform', 'translate(525, 150)')
        .attr('id', 'legend');
    legend.selectAll('.colorbar')
        .data(d3.range(9))
        .enter().append('svg:rect')
        .attr('y', function (d) { return d * 20 + 'px'; })
        .attr('height', '20px')
        .attr('width', '20px')
        .attr('x', '0px')
        .attr("class", function (d) { return "q" + d + "-9"; });
    var legendScale = d3.scaleLinear()
        .domain([0, d3.max(csv, function (e) { return +e.POP; })])
        .range([0, 9 * 20]);
    var legendAxis = svg.append("g")
        .attr('transform', 'translate(550, 150)')
        .call(d3.axisRight(legendScale).ticks(6));
    csv.forEach(function (e, i) {
        d3.select("#d" + e.CODE_DEPT)
            .attr("class", function (d) { return "department q" + quantile(+e.POP) + "-9"; })
            .on("mouseover", function (d) {
                div.transition()
                    .duration(200)
                    .style("opacity", .9);
                div.html("<b>Région : </b>" + e.NOM_REGION + "<br>"
                    + "<b>Département : </b>" + e.NOM_DEPT + "<br>"
                    + "<b>Nombre de radars : </b>" + e.POP + "<br>")
                    .style("left", (d3.event.pageX + 30) + "px")
                    .style("top", (d3.event.pageY - 30) + "px");
            })
            .on("mouseout", function (d) {
                div.style("opacity", 0);
                div.html("")
                    .style("left", "-500px")
                    .style("top", "-500px");
            })
            //Ajout de l'élément on click pour passer en barchat 

            .on('click', function (d) {
                tr.push(e.POP); tr2.push(e.NOM_DEPT);;  // On créé deux tableaux  , un de nnom de regionet l'autre de population
                var depart = e.NOM_DEPT;
                console.log("Le département est :", deps.NOM_DEPT);
                //////////////////////////////////
                var result = {};
                tr2.forEach((key, i) => result[key] = tr[i]);
                //console.log(result);  //Les trois lignes plus hautes permettent de combiner  les deux arrays pour en faire un objet 
                cles = Object.keys(result);
                valeurs = Object.values(result)
                console.log(cles);
                console.log(valeurs);

                donut(result)


                // data = d3.entries(result) // command en d3 pour reccupérer les valeurs de l'objet 

                // console.log(Object.values(result));

                //La commande Object.keys(result) permet de recupere les clés de l'objet ; object.values(result) permet de recuperer les valeurs 



                ////////////////////////////////////
                //Je cré mon barchart sur le onclick  
                // var barWidth = (svgWidth / 10); // epaisseur de la barre 
                //  var barChart = svg2.selectAll("rect")
                //    .data() // mon data c'est l'élement tr(array) dans lequel je stock le nombre de radar 
                //     .enter()
                //     .append("rect")
                //     .attr("class","bar")
                //     .attr("y", function (d) {
                //         return  svgHeight - d  ;
                //     })
                //     .attr("height", function (d) {
                //         return  ( d );
                //     })


                //     .attr("width", barWidth - barPadding)
                //     .attr("transform", function (d, i) {
                //          var translate = [barWidth * i, 0];
                //         return "translate(" + translate + ")";
                //         console.log(translate);
                //     });
                //axis x et y un peu plus haut pour lecheck


                ///


            });


        ;
    });
});
d3.select("select").on("change", function () {
    d3.selectAll("svg").attr("class", this.value);
});
d3.select("#data").on("change", function () {
    selectedData = this.value;

    quantile = d3.scale.scaleQuantile
        .domain([0, d3.max(csv, function (e) { return +e[selectedData]; })])
        .range(d3.range(9));

    legendScale.domain([0, d3.max(csv, function (e) { return +e[selectedData]; })]);
    legendAxis.call(d3.axisRight(legendScale).ticks(6));

    csv.forEach(function (e, i) {
        d3.select("#d" + e.CODE_DEPT)
            .attr("class", function (d) { return "department q" + quantile(+e[selectedData]) + "-9"; });
    });
});

